import Home from "../pages/HomePage/HomePage";
import Cart from "../pages/CartPage/CartPage";
import CategoryProduct from "../pages/CategoryProductPage/CategoryProductPage";
import ProductDetail from "./ProductDetailPage/ProductDetailPage";
import Search from "./SearchPage/SearchPage";

export { Home, CategoryProduct, ProductDetail, Cart, Search };
