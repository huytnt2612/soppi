import "./App.scss";

import { BrowserRouter, Routes, Route } from "react-router-dom";

import Header from "./components/Header/Header";
import Sidebar from "./components/Sidebar/Sidebar";
import Footer from "./components/Footer/Footer";

import {
  Home,
  CategoryProduct,
  ProductDetail,
  Cart,
  Search,
} from "./pages/index";

import store from "./store/store";
import { Provider } from "react-redux";

function App() {
  return (
    <>
      <div className="App">
        <Provider store={store}>
          <BrowserRouter>
            <Header />
            <Sidebar />
            <Routes>
              <Route path="/" element={<Home />} />

              <Route path="/product/:id" element={<ProductDetail />} />

              <Route path="/category/:category" element={<CategoryProduct />} />

              <Route path="/cart" element={<Cart />} />

              <Route path="/search/:searchTerm" element={<Search />} />
            </Routes>
            <Footer />
          </BrowserRouter>
        </Provider>
      </div>
    </>
  );
}

export default App;
