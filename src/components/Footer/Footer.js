import React from "react";
import "./Footer.scss";

import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <>
      <div className="footer">
        <div className="container py-4">
          <div className=" content">
            <Link to="/" className="text-uppercase">
              privacy policy
            </Link>
            <div className="vert-line"></div>
            <Link to="/" className="text-uppercase">
              term of service
            </Link>
            <div className="vert-line"></div>
            <Link to="/" className="text-uppercase">
              About Xopbe.
            </Link>
          </div>
          <span className="text-white copyright-text text-manrope fs-14 fw-3">
            &copy; 2022 Xopbe. All Rights Reserved.
          </span>
        </div>
      </div>
    </>
  );
};

export default Footer;
