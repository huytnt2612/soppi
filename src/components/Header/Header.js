import React from "react";
import "./Header.scss";

import { Link } from "react-router-dom";
import Navbar from "./../Navbar/Navbar";

const Header = () => {
  return (
    <>
      <div className="header">
        <div className="container">
          <div className="header-cnt">
            <div className="header-top">
              <div className="header-cnt-l">
                <ul>
                  <li>
                    <Link to="./hehe"> Seller Center</Link>
                  </li>
                  <li className="vert-line"></li>
                  <li>
                    <Link to="./hehe"> Download</Link>
                  </li>
                  <li className="vert-line"></li>
                  <div className="follow">
                    <span>Follow us on</span>
                    <ul>
                      <li>
                        <a href="www.facebook.com">
                          <i className="fab fa-facebook"></i>
                        </a>
                      </li>
                      <li>
                        <a href="www.instagram.com">
                          <i className="fab fa-instagram"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </ul>
              </div>
              <div className="header-cnt-r">
                <ul>
                  <li>
                    <Link to="/">
                      <span>
                        <i className="fa-solid fa-circle-question"></i>
                      </span>
                      <span>Support</span>
                    </Link>
                  </li>
                  <li className="vert-line"></li>
                  <li>
                    <Link to="/">
                      <span>Register</span>
                    </Link>
                  </li>
                  <li className="vert-line"></li>
                  <li>
                    <Link to="/">
                      <span>Log in</span>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
            <div className="header-bottom">
              <Navbar />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
