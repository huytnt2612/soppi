import { configureStore } from "@reduxjs/toolkit";
import sidebarReducer from "./sidebarSlide";
import categoryReducer from "./categorySlice";
import productSlice from "./productSlice";
import cartReducer from "./cartSlice";

const store = configureStore({
  reducer: {
    sidebar: sidebarReducer,
    category: categoryReducer,
    product: productSlice,
    cart: cartReducer,
  },
});

export default store;
